import { SCREENS } from '../../App'
import { navigationType } from '../../interface/screen.props.interface'

export function curriedGoToScreen(navigation: navigationType) {
	return function (screenEnums: keyof typeof SCREENS, props?: any) {
		navigation.navigate(screenEnums, props)
	}
}
