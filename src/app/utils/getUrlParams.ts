// import { URL, URLSearchParams } from 'react-native-url-polyfill'

export function getUrlParams(url: string, param: string) {
	// const { search } = new URL(url)
	// const urlSearchParams = new URLSearchParams(search)
	// return urlSearchParams.get(param)
	const match = url.match('[?&]' + param + '=([^&]+)')
	return match ? match[1] : null
}
