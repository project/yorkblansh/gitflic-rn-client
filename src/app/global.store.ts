import { GitFlic } from 'gitflic-api'

export class GlobalStore {
	private static API_Map: Map<string, GitFlic['API']> = new Map()

	private static GitflicAuthMap: Map<string, string> = new Map([
		['accesToken', ''],
		['expires', ''],
		['refreshToken', ''],
	])

	static setAccesToken(accesToken: string) {
		this.GitflicAuthMap.set('accesToken', accesToken)
		return this
	}

	static setExpires(expires: string) {
		this.GitflicAuthMap.set('expires', expires)
		return this
	}

	static setRefreshToken(refreshToken: string) {
		this.GitflicAuthMap.set('refreshToken', refreshToken)
		return this
	}

	static getAccesToken() {
		return this.GitflicAuthMap.get('accesToken')
	}

	static setGitFlicInstance(API: GitFlic['API']) {
		this.API_Map.set('current', API)
		return this
	}

	static getGitFlicInstance() {
		return this.API_Map.get('current')
	}
}
