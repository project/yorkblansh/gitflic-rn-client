import { User } from 'gitflic-api'

type CachedUserCredentials = User

export class AccountDataStore {
	private static map: Map<'user', User> = new Map<'user', User>([
		[
			'user',
			{
				avatar: '',
				fullName: '',
				id: '',
				name: '',
				surname: '',
				username: '',
			},
		],
	])

	static set(userCredentials: User): AccountDataStore {
		AccountDataStore.map.set('user', userCredentials)
		return AccountDataStore
	}

	static get(): CachedUserCredentials {
		return AccountDataStore.map.get('user')
	}
}
