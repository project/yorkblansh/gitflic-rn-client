import { GitFlic } from 'gitflic-api'
import { gitflic_api_url } from './gitflic_credentials.json'
import { clientId, gitflic_oauth_url } from './oauth_credentials.json'
import md5 from 'md5'
import { GlobalStore } from '../global.store'

const OAUTH = new GitFlic.OAUTH({
	clientId,
	gitflic_oauth_url,
	scope: [
		'USER_READ',
		'USER_WRITE',
		'PROJECT_READ',
		'PROJECT_WRITE',
		'PROJECT_EDIT',
	],
	state: md5(new Date().toString()),
})

export function getNewGitFlicInstance(gitflic_token?: string) {
	return new GitFlic({
		gitflic_api_url,
		gitflic_token: gitflic_token ? gitflic_token : GlobalStore.getAccesToken(),
	})
}

export { OAUTH }
