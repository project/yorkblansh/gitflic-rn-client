import { getNewGitFlicInstance } from '../GitFlic/gitflicInstances'
import { GlobalStore } from '../global.store'
import { getUrlParams } from '../utils/getUrlParams'
import { fetchDeepLink } from './fetchDeepLink'
import { gitflic_oauth_deeplink } from '../GitFlic/oauth_credentials.json'
import { AccountDataStore } from '../account.data.store'

export async function performAuth(urlWithAccesCode: string) {
	return new Promise<boolean>(async function (resolve, reject) {
		const params = { code: getUrlParams(urlWithAccesCode, 'code') }
		console.log(urlWithAccesCode)
		fetchDeepLink(gitflic_oauth_deeplink, params)
			.then((response) => {
				const isAuthCorrect =
					response && response.data && response.data.accessToken
				const { accessToken, refreshToken, expires } = response.data

				const gfa = getNewGitFlicInstance(accessToken).API

				GlobalStore.setAccesToken(accessToken)
					.setExpires(expires)
					.setRefreshToken(refreshToken)
					.setGitFlicInstance(gfa)

				gfa.user
					.get('currentUser')
					.by({})
					.then(({ data: user }) => AccountDataStore.set(user))
					.catch((error) => reject(error))
					.finally(() => {
						isAuthCorrect ? resolve(true) : reject(false)
					})
			})
			.catch((er) => console.log(er))
	})
}
