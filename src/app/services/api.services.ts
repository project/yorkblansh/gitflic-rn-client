import { IssueDiscussionQueryProps, Pagination } from 'gitflic-api'
import { UserSpecification } from 'gitflic-api/lib/api/project/typings/project.api.typings'
import { GlobalStore } from '../global.store'
import { FakeApiServices } from './fakeData/fake.api.services'
import { APP_MODE } from '../../mode'
const gf = () => GlobalStore.getGitFlicInstance()

export class _GitflicServices_ {
	static getCurrentUser() {
		return gf().user.get('currentUser').by({})
	}

	static getSingleProject(options: { params: UserSpecification }) {
		return gf().project.get('singleProject').by(options)
	}

	static getMyProjectList(paginationParams?: Pagination['params']) {
		return gf().project.get('myProjects').by({})
	}

	static getAllProjectList(paginationParams?: Pagination['params']) {
		return gf().project.get('allProjects').by({})
	}

	static getIssues(props: { idqp: IssueDiscussionQueryProps }) {
		return gf().issueDiscussion.getIssueDiscussionList(props.idqp)
	}
}

export const ApiServices =
	APP_MODE === 'normal' ? _GitflicServices_ : FakeApiServices
