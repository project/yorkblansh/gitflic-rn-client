import axios, { AxiosResponse } from 'axios'
import { AccessToken } from 'gitflic-api/lib/oauth/interfaces/oauth.acces.token.interface'

export function fetchDeepLink(baseUrl: string, params: { code: string }) {
	return new Promise<AxiosResponse<AccessToken>>(function (resolve, reject) {
		axios
			.get<AccessToken>(baseUrl, { params })
			.then((response) => resolve(response))
			.catch((error) => reject(error))
	})
}
