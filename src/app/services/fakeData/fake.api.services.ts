import { AxiosResponse } from 'axios'
import {
	EntitieList,
	IssueDiscussionQueryProps,
	Pagination,
	Project,
	User,
} from 'gitflic-api'
import { UserSpecification } from 'gitflic-api/lib/api/project/typings/project.api.typings'
import { _GitflicServices_ } from '../api.services'

//entitie templates
const user: User = {
	avatar: '',
	fullName: '',
	id: '',
	name: '',
	surname: '',
	username: '',
}

const project: Project = {
	alias: 'some-project',
	description: 'some project description',
	httpTransportUrl: 'https://somedomain.com',
	id: 'someID',
	ownerAlias: 'me',
	private: false,
	selectorAlias: 'adwaldkjwlkjd',
	selectorColor: 'adawdlkjalk',
	selectorHash: 'awdjlkawjd',
	selectorId: 'awdjdlk',
	selectorOwnerAlias: 'me',
	selectorTitle: 'awdkjl',
	sshTransportUrl: 'awdjkjl',
	title: 'SomeTitle',
}

function entitieList<T extends Project[] | User[]>(
	entitie: T,
): EntitieList<'userList'> | EntitieList<'projectList'> {
	return {
		_embedded: {
			userList: entitie as User[],
			projectList: entitie as Project[],
		},
		page: {
			number: 1,
			size: 1,
			totalElements: 1,
			totalPages: 1,
		},
	}
}

const axiosResponse = {
	config: {},
	headers: {},
	status: 200,
	statusText: 'ok',
}

//Fake Api(Gitdlic) Services
export class FakeApiServices extends _GitflicServices_ {
	static getCurrentUser() {
		return new Promise<AxiosResponse<User>>((resolve, reject) => {
			resolve({ data: user, ...axiosResponse })
			reject('some error while getting user')
		})
		// return gf().user.get('currentUser').by({})
	}

	static getSingleProject(options: { params: UserSpecification }) {
		return new Promise<AxiosResponse<Project>>((resolve, reject) => {
			resolve({ data: project, ...axiosResponse })
			reject('some error while getting project')
		})
		// return gf().project.get('singleProject').by(options)
	}

	static getMyProjectList(paginationParams?: Pagination['params']) {
		return new Promise<AxiosResponse<EntitieList<'projectList'>>>(
			(resolve, reject) => {
				resolve({
					data: entitieList<Project[]>([project, project]),
					...axiosResponse,
				})
				reject('some error while getting project')
			},
		)
		// return gf().project.get('myProjects').by({})
	}

	static getAllProjectList(paginationParams?: Pagination['params']) {
		return new Promise<AxiosResponse<EntitieList<'projectList'>>>(
			(resolve, reject) => {
				resolve({
					data: entitieList<Project[]>([project, project]),
					...axiosResponse,
				})
				reject('some error while getting project')
			},
		)
		// return gf().project.get('allProjects').by({})
	}

	static getIssues(props: { idqp: IssueDiscussionQueryProps }) {
		return new Promise<AxiosResponse<any>>((resolve, reject) => {
			resolve({
				//TODO доабвить фейк юшью
				data: {},
				...axiosResponse,
			})
			reject('some error while getting project')
		})
		// return gf().issueDiscussion.getIssueDiscussionList(props.idqp)
	}
}
