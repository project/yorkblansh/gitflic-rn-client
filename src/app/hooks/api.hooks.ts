/* eslint-disable react-hooks/rules-of-hooks */
import { ApiServices } from '../services/api.services'
import { AxiosResponse } from 'axios'
import { useEffect, useState } from 'react'
import { IssueDiscussionQueryProps } from 'gitflic-api'

type EndType<T> = T extends (x?: any) => Promise<AxiosResponse<infer E>> ? E : T

export class ApiHooks {
	static useCurrentUser() {
		return ApiHooks.useApi(ApiServices.getCurrentUser)
	}

	static useSingleProject(
		...props: Parameters<typeof ApiServices.getSingleProject>
	) {
		return ApiHooks.useApi(ApiServices.getSingleProject, ...props)
	}

	static useMyProjectList() {
		//...props: Parameters<typeof ApiServices.getMyProjectList>
		return ApiHooks.useApi(
			ApiServices.getMyProjectList,
			//, props[0]
		)
	}

	static useAllProjectList() {
		//...props: Parameters<typeof ApiServices.getMyProjectList>
		return ApiHooks.useApi(
			ApiServices.getAllProjectList,
			//, props[0]
		)
	}

	static useIssues(props: { idqp: IssueDiscussionQueryProps }) {
		return ApiHooks.useApi(ApiServices.getIssues, props)
	}

	private static useApi<
		P extends (x?: any) => Promise<AxiosResponse<any, any>>,
	>(servicePromise: P, serviceProps?: any) {
		const [data, setData] = useState<EndType<P>>()
		const [isFetching, setFetching] = useState(true)

		useEffect(() => {
			servicePromise(serviceProps)
				.then(({ data: userData }) => setData(userData))
				.catch(error => console.dir(error))
				.finally(() => setFetching(false))
		}, [setFetching, setData, servicePromise, serviceProps])
		return [data, isFetching] as const
	}
}
