import React from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { createNativeStackNavigator } from '@react-navigation/native-stack'
import { HomeScreen } from './screens/home/HomeScreen'
import { ProfileScreen } from './screens/profile/ProfileScreen'

//TODO сделать отдельный логин скрин для web
// import { LoginScreenAndroid } from './screens/auth/login/login.android/LoginScreenAndroid'

import { MyProjectListScreen } from './screens/project/MyProjectListScreen'

//font awesome stuff
import { library } from '@fortawesome/fontawesome-svg-core'
import { fab } from '@fortawesome/free-brands-svg-icons'
import { fas } from '@fortawesome/free-solid-svg-icons'
import { far } from '@fortawesome/free-regular-svg-icons'
import { AuthScreen } from './screens/auth/AuthScreen'
import { ProjectScreen } from './screens/project/ProjectScreen'
import { LoginScreen } from './screens/auth/login/login.screen'
import { APP_MODE } from './mode'
import { SettingsScreen } from './screens/settings/settings.screen'

//applying font awesome for whole project
library.add(fab, fas, far)

const Stack = createNativeStackNavigator()
const headerShown = false

export enum SCREENS {
	//Bootstrap screen
	Auth = 'Auth',

	//Main Screens
	Home = 'Home',
	Profile = 'Profile',
	Login = 'Login',
	Project = 'Project',

	//Special Screens
	ProjectList = 'ProjectList',
	TeamList = 'TeamList',
	CompanyList = 'CompanyList',
	Settings = 'Settings',
}

const App = () => (
	<NavigationContainer>
		<Stack.Navigator>
			{APP_MODE === 'normal' && (
				<>
					<Stack.Screen
						name={SCREENS.Auth.toString()}
						component={AuthScreen}
						options={{ headerShown }}
					/>

					<Stack.Screen
						name={SCREENS.Login.toString()}
						component={LoginScreen}
						options={{ headerShown }}
					/>
				</>
			)}

			<Stack.Screen
				name={SCREENS.Profile.toString()}
				component={ProfileScreen}
				options={{ headerShown }}
			/>
			<Stack.Screen
				name="Home"
				component={HomeScreen}
				options={{ headerShown }}
			/>

			<Stack.Screen
				name={SCREENS.ProjectList.toString()}
				component={MyProjectListScreen}
				options={{ headerShown }}
			/>

			<Stack.Screen
				name={SCREENS.Project.toString()}
				component={ProjectScreen}
				options={{ headerShown }}
			/>

			<Stack.Screen
				name={SCREENS.Settings.toString()}
				component={SettingsScreen}
				options={{ headerShown }}
			/>
		</Stack.Navigator>
	</NavigationContainer>
)

export default App
