import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { useNavigation } from '@react-navigation/native'
import { EntitieList } from 'gitflic-api'
import React from 'react'
import {
	Pressable,
	ScrollView,
	StyleSheet,
	Text,
	useWindowDimensions,
	View,
} from 'react-native'
import { curriedGoToScreen } from '../../app/utils/goToScreen'

interface Props {
	entitieList: EntitieList<'projectList'>
}

export const ProjectList = ({
	entitieList: {
		_embedded: { projectList },
		//TODO реализовать переход по страницам
		page: { number, size, totalElements, totalPages },
	},
}: Props) => {
	const navigation = useNavigation()

	//TODO реализовать скрин для конкретного проекта
	const goToScreen = curriedGoToScreen(navigation)
	const { height } = useWindowDimensions()
	const privateProjectIcon = (
		<FontAwesomeIcon size={14} color="#3d3d3d" icon={['fas', 'lock']} />
	)

	return (
		<ScrollView style={{ maxHeight: height - 15 }}>
			{projectList.map(project => {
				const { private: isPrivate, description, title, alias } = project
				return (
					<View key={title} style={styles.itemWrapper}>
						<Pressable
							onPress={() => {
								console.log(`${title} presed`)

								goToScreen('Project', { project })
							}}>
							<View style={styles.item}>
								<View style={styles.itemTitle}>
									{isPrivate && ( // Private Icon
										<View style={styles.itemTitleIcon}>
											{privateProjectIcon}
										</View>
									)}
									<Text
										// Title
										style={styles.itemTitleText}>
										{title}
									</Text>
								</View>

								{title !== alias && ( // Alias
									<View style={styles.itemAlias}>
										<Text style={styles.itemAliasText}>{alias}</Text>
									</View>
								)}

								{description !== '' && ( // Description
									<View style={styles.itemDescription}>
										<Text style={styles.itemDescriptionText}>
											{description}
										</Text>
									</View>
								)}
							</View>
						</Pressable>
					</View>
				)
			})}
		</ScrollView>
	)
}

const styles = StyleSheet.create({
	itemWrapper: {
		paddingHorizontal: 15,
		display: 'flex',
		backgroundColor: '#fff',
		borderColor: 'transparent',
		borderWidth: 0,
		borderTopWidth: 0,
	},

	item: {
		backgroundColor: '#fff',
		paddingVertical: 12,

		borderBottomWidth: 2,
		borderBottomColor: '#d1d1d1',
	},

	itemTitle: {
		display: 'flex',
		flexDirection: 'row',
		paddingHorizontal: 8,
		paddingVertical: 3,
	},
	itemTitleText: {
		color: '#000000',
		display: 'flex',
		fontSize: 18,
		fontWeight: '600',
	},
	itemTitleIcon: {
		display: 'flex',
		paddingRight: 8,
		paddingVertical: 6,
	},

	itemAlias: {
		display: 'flex',
		flexDirection: 'row',
		paddingHorizontal: 8,
	},
	itemAliasText: {
		paddingBottom: 2,
		backgroundColor: '#d3d3d3',
		color: '#000000',
		display: 'flex',
		fontSize: 14,
		paddingHorizontal: 6,
		borderRadius: 5,
	},

	itemDescription: {
		display: 'flex',
		paddingHorizontal: 6,
		paddingTop: 2,
	},

	itemDescriptionText: {
		color: '#444444',
	},
})
