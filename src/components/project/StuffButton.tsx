import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import React from 'react'
import { StyleSheet, View, Text, StyleProp, ViewStyle } from 'react-native'

interface IconMapProps {
	pressed: boolean
}

const ICON_MAP = {
	issue: (
		<FontAwesomeIcon size={20} color="#fff" icon={['far', 'circle-dot']} />
	),
	pullRequest: (
		<FontAwesomeIcon
			size={20}
			color="#fff"
			icon={['fas', 'code-pull-request']}
		/>
	),
	release: <FontAwesomeIcon size={20} color="#fff" icon={['fas', 'box']} />,
	project: (
		<FontAwesomeIcon size={20} color="#fff" icon={['fas', 'book-bookmark']} />
	),

	company: (
		<FontAwesomeIcon size={20} color="#fff" icon={['fas', 'building']} />
	),
	team: (
		<FontAwesomeIcon size={20} color="#fff" icon={['fas', 'people-group']} />
	),
	branch: (
		<FontAwesomeIcon size={20} color="#fff" icon={['fas', 'code-branch']} />
	),
	code: <FontAwesomeIcon size={20} color="#fff" icon={['fas', 'code']} />,
	currentMainBranch: (
		<FontAwesomeIcon size={20} color="#fff" icon={['fas', 'code-branch']} />
	),
}

interface Props {
	title: string
	icon: keyof typeof ICON_MAP
	color: string
	count?: number
	size?: number
	children?: JSX.Element
}

export const StuffButton = ({
	title,
	icon,
	color,
	count,
	size,
	children,
}: Props) => {
	return (
		<View style={styles.wrapper}>
			<View style={{ display: 'flex', flexDirection: 'row', width: 'auto' }}>
				<View
					style={{
						display: 'flex',
						flexDirection: 'row',
						width: '15%',
						// backgroundColor: '#59ccb3',
						paddingHorizontal: 15,
						marginRight: 10,
						paddingVertical: 10,
					}}>
					<View
						style={{
							backgroundColor: color,
							...styles.issueBtnIcon,
							maxHeight: size,
							maxWidth: size,
						}}>
						{ICON_MAP[icon]}
					</View>
				</View>

				<View
					style={{
						// backgroundColor: '#d12828',
						display: 'flex',
						flexDirection: 'column',
						width: '70%',
						height: 'auto',
						alignSelf: 'center',
						justifyContent: 'center',
						// alignContent:'center'
					}}>
					<Text style={{ color: '#000', fontSize: 20 }}>{title}</Text>
				</View>
			</View>

			<View
				style={{
					// backgroundColor: '#d12828',
					display: 'flex',
					flexDirection: 'column',
					width: '100%',
					height: 'auto',
					alignSelf: 'center',
					justifyContent: 'center',
					alignItems: 'center',
					// alignContent:'center'
				}}>
				{children}
				{/* <Text style={{ color: '#686868', fontSize: 22 }}>{count}</Text> */}
			</View>
		</View>
	)
}

const styles = StyleSheet.create({
	issueBtnIcon: {
		// backgroundColor: '#41ac21',
		display: 'flex',
		flexDirection: 'column',
		justifyContent: 'center',
		alignItems: 'center',
		// marginHorizontal: 15,
		// marginVertical: 10,
		width: 35,
		height: 35,
		borderRadius: 4,
	},
	wrapper: {
		// width: 10,
		backgroundColor: 'transparent',
		display: 'flex',
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignContent: 'flex-start',
		alignItems: 'flex-start',
		// borderBottomColor: '#969696',
		// borderBottomWidth: 2,
	},
})
