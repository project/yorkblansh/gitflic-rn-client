/* eslint-disable react-native/no-inline-styles */
import React from 'react'
import { Image } from 'react-native'

interface Props {
	width: number
	height: number
	imageUrl: string
}

export const UserAvatar = ({ imageUrl, width, height }: Props) => (
	<Image
		source={{ uri: imageUrl }}
		style={{ height, width, borderRadius: 240 }}
	/>
)
