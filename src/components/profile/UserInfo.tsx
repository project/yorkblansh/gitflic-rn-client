import { User } from 'gitflic-api'
import React from 'react'
import { StyleSheet, Text, useWindowDimensions, View } from 'react-native'
import { UserAvatar } from './UserAvatar'

interface Props {
	isFetching: boolean
}

export const UserInfo = ({
	name,
	surname,
	username,
	avatar,
	isFetching,
}: User & Props) => {
	const { width } = useWindowDimensions()
	return (
		<View>
			{isFetching ? (
				<Text>Loading...</Text>
			) : (
				<>
					<UserAvatar height={width / 2} width={width / 2} imageUrl={avatar} />

					<View
						// Name & Surname
						style={styles.nameSurname}>
						<Text style={styles.font_20}>{name} </Text>
						<Text style={styles.font_20}>{surname}</Text>
					</View>

					<View
						// @username
						style={styles.username}>
						<Text style={styles.font_17}>@{username} </Text>
					</View>
				</>
			)}
		</View>
	)
}

const styles = StyleSheet.create({
	nameSurname: {
		display: 'flex',
		flexDirection: 'row',
		alignItems: 'center',
		backgroundColor: '#ffffff',
		paddingHorizontal: 10,
		borderRadius: 5,
		marginTop: 5,
		borderColor: '#c6c9d3',
		borderWidth: 1,
	},
	font_20: { color: '#000000', fontSize: 20 },

	username: {
		display: 'flex',
		flexDirection: 'row',
		alignItems: 'center',
		backgroundColor: '#ffffff',
		paddingHorizontal: 10,
		borderRadius: 5,
		marginVertical: 4,

		borderColor: '#c6c9d3',
		borderWidth: 1,
	},
	font_17: { color: '#000000', fontSize: 17 },
})
