import React from 'react'
import { SCREENS } from '../../../App'
import { BottomBar } from './BottomBar/BottomBar'
import { HeaderBar } from './HeaderBar/HeaderBar'

interface Props {
	screen: keyof typeof SCREENS
	props?: { [x: string]: any }
}

export const NavigationBar = ({ screen, props }: Props) => {
	const isProfileScreen = screen === 'Profile'
	const isHomeScreen = screen === 'Home'
	const isProjectScreen = screen === 'Project'

	if (isHomeScreen) return <BottomBar {...{ screen }} />
	else if (isProfileScreen) return <BottomBar {...{ screen }} />
	else if (isProjectScreen) return <HeaderBar {...{ props }} screen="Project" />
	else return <HeaderBar {...{ props }} screen="ProjectList" />

	// return isHomeScreen || isProfileScreen ? (
	// 	<BottomBar {...{ screen }} />
	// ) : isProjectScreen ? (
	// 	<HeaderBar {...{ props }} screen="Project" />
	// ) : (
	// 	<HeaderBar {...{ props }} screen="ProjectList" />
	// )
}
