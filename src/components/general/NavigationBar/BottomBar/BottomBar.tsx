import { useNavigation } from '@react-navigation/native'
import React from 'react'
import { StyleSheet, View } from 'react-native'
import { SCREENS } from '../../../../App'
import { curriedGoToScreen } from '../../../../app/utils/goToScreen'
import { IconNavigationButton } from '../../Buttons/IconNavigationButton/IconNavigationButton'

interface Props {
	screen: keyof typeof SCREENS
}

export const BottomBar = ({ screen: specScreen }: Props) => {
	const navigation = useNavigation()
	const goToScreen = curriedGoToScreen(navigation)

	const isProfileScreen = specScreen === 'Profile'
	const isHomeScreen = specScreen === 'Home'

	return (
		<View style={styles.wrapper}>
			<View style={styles.bottomBar}>
				<View
					style={styles.navigationBtn}
					onTouchStart={() => goToScreen('Home')}>
					<IconNavigationButton
						title="Главная"
						color={isHomeScreen ? '#007bff' : '#000'}
						icon={['fas', 'house']}
					/>
				</View>

				<View
					style={styles.navigationBtn}
					onTouchStart={() => goToScreen('Profile')}>
					<IconNavigationButton
						title="Профиль"
						color={isProfileScreen ? '#007bff' : '#000'}
						icon={[isProfileScreen ? 'fas' : 'far', 'user']}
					/>
				</View>
			</View>
		</View>
	)
}

const styles = StyleSheet.create({
	wrapper: {
		display: 'flex',
		alignItems: 'stretch',
		justifyContent: 'flex-start',
		flexDirection: 'column',
		height: 60,
		width: 'auto',
	},

	bottomBar: {
		position: 'relative',
		width: 'auto',
		height: '100%',
		backgroundColor: 'white',
		flexDirection: 'row',
		alignItems: 'center',
		alignContent: 'center',
		justifyContent: 'space-around',
		paddingHorizontal: 0,
		borderColor: '#000',
		borderWidth: 0,
		paddingTop: 5,
	},

	navigationBtn: {
		backgroundColor: 'transparent',
		width: '30%',
	},
})
