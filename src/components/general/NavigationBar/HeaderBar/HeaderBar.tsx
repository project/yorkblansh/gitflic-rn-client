/* eslint-disable dot-notation */
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { useNavigation } from '@react-navigation/native'
import { Project } from 'gitflic-api'
import React from 'react'
import { View, Text, StyleSheet } from 'react-native'
import { SCREENS } from '../../../../App'
import { AccountDataStore } from '../../../../app/account.data.store'
import { IconNavigationButton } from '../../Buttons/IconNavigationButton/IconNavigationButton'

interface Props {
	screen: keyof typeof SCREENS
	props?: { [x: string]: any }
}

export const HeaderBar = ({ screen, props }: Props) => {
	const navigation = useNavigation()
	const isCurrentUserProjectListScreen = screen === 'ProjectList'
	const isProjectScreen = screen === 'Project'
	const isCurrentUserProfileScreen = screen === 'Profile'

	const shareIcon = (
		<FontAwesomeIcon size={22} color="#3d3d3d" icon={['fas', 'share-nodes']} />
	)

	const fnProjectIcon = (
		<FontAwesomeIcon
			size={20}
			color="#3d3d3d"
			icon={['fas', 'ellipsis-vertical']}
		/>
	)
	const filterIcon = (
		<FontAwesomeIcon size={20} color="#3d3d3d" icon={['fas', 'filter']} />
	)
	// console.log(props)

	return (
		<View style={styles.wrapper}>
			{!isCurrentUserProfileScreen && (
				<View onTouchStart={() => navigation.goBack()} style={styles.backBtn}>
					<IconNavigationButton
						iconSize={15}
						color={'#000'}
						icon={['fas', 'long-arrow-left']}
					/>
				</View>
			)}

			{isCurrentUserProjectListScreen && (
				<>
					<View
						style={{
							// backgroundColor: '#eb7c7c',
							height: '100%',
							width: '70%',
							display: 'flex',
							flexDirection: 'column',
							justifyContent: 'center',
							// flexDirection: 'row',
							// justifyContent: 'space-around',
							// alignSelf: 'stretch',
							// elevation: 5,
						}}>
						<Text style={styles.username}>
							{AccountDataStore.get().username}
						</Text>
						<Text style={styles.title}>Мои проекты</Text>
					</View>

					<View style={styles.fnBtn}>{filterIcon}</View>
				</>
			)}

			{isProjectScreen &&
				(() => {
					const project = props['project'] as Project
					const { alias } = project
					return (
						<>
							<View
								style={{
									// backgroundColor: '#db4f4f',
									height: '100%',
									width: '70%',
									display: 'flex',
									flexDirection: 'column',
									justifyContent: 'center',
									// flexDirection: 'row',
									// justifyContent: 'space-around',
									// alignSelf: 'stretch',
									// elevation: 5,
								}}>
								<Text style={{ color: '#000' }}>{alias}</Text>
							</View>

							<View style={styles.fnBtn}>
								{fnProjectIcon}
								{/* <Button title="jk" /> */}
							</View>
						</>
					)
				})()}

			{isCurrentUserProfileScreen && (
				<>
					<View></View>
					<View style={{ display: 'flex', flexDirection: 'row' }}>
						<View style={styles.fnBtn}>{shareIcon}</View>
						<View style={styles.fnBtn}>{fnProjectIcon}</View>
					</View>
				</>
			)}
		</View>
	)
}

const styles = StyleSheet.create({
	wrapper: {
		display: 'flex',
		alignItems: 'center',
		justifyContent: 'space-between',
		flexDirection: 'row',
		height: 60,
		width: '100%',
		borderBottomWidth: 0.4,
		borderBottomColor: '#c2c2c2',
	},

	backBtn: {
		// left: 50,
		display: 'flex',
		// flexDirection: 'column',
		width: '15%',
		height: '100%',
		// backgroundColor: '#b43333',
	},

	username: {
		color: '#000',
		backgroundColor: 'transparent',
		fontWeight: '400',
		fontSize: 15,
	},
	title: {
		color: '#000',
		backgroundColor: 'transparent',
		fontSize: 20,
		fontWeight: '700',
	},

	fnBtn: {
		// backgroundColor: '#4dd36e',
		// height: '100%',
		width: 45,
		display: 'flex',
		flexDirection: 'column',
		alignItems: 'center',
		justifyContent: 'center',
		alignSelf: 'center',

		// alignContent: 'center',
		// paddingEnd: 5,
		// justifyContent: 'center',
		// alignSelf: 'stretch',
		// flexDirection: 'row',
		// justifyContent: 'flex-end',
		// alignSelf: 'stretch',
	},
})
