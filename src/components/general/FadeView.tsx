import { useEffect, useRef } from 'react'
import { Animated, RegisteredStyle, ViewStyle } from 'react-native'
import React from 'react'

interface Props {
	useEffect: boolean
	style?:
		| false
		| RegisteredStyle<ViewStyle>
		| Animated.Value
		| Animated.AnimatedInterpolation
		| Animated.WithAnimatedObject<ViewStyle>
		| Animated.WithAnimatedArray<any>
	duration: number
	children: JSX.Element
}

export const FadeView = (props: Props) => {
	const fadeAnim = useRef(new Animated.Value(1)).current // Initial value for opacity: 0

	useEffect(() => {
		props.useEffect &&
			Animated.timing(fadeAnim, {
				useNativeDriver: true,
				toValue: 0,
				duration: props.duration,
			}).start()
	}, [fadeAnim, props.duration, props.useEffect])

	return (
		<Animated.View // Special animatable View
			style={{
				...(props.style as object),
				opacity: fadeAnim, // Bind opacity to animated value
			}}>
			{props.children}
		</Animated.View>
	)
}
