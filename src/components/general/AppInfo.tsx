import { StyleSheet, Text } from 'react-native'
import React from 'react'

export const FooterAppInfo = () => (
	<Text style={styles.origin}>GitFlic Mobile - open source community app</Text>
)

const styles = StyleSheet.create({
	origin: {
		fontSize: 16,
		color: '#6e6e6e',
		textAlign: 'center',
	},
})
