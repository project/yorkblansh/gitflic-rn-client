import { IconProp } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import React from 'react'
import { StyleSheet, Text, View } from 'react-native'

interface Btn {
	color: '#007bff' | '#000'
	icon: IconProp
	title?: string | undefined
	iconSize?: number
}

const NavBtnIconSize: number = 30

export const IconNavigationButton = ({ color, icon, title, iconSize }: Btn) => (
	<View style={styles.navBtn}>
		<FontAwesomeIcon
			{...{
				color,
				icon,
				style: { alignContent: 'center' },
				size: iconSize ? iconSize : NavBtnIconSize,
			}}
		/>
		{title && <Text style={{ ...styles.navBtnTitle, color }}>{title}</Text>}
	</View>
)

const styles = StyleSheet.create({
	navBtn: {
		display: 'flex',
		flexDirection: 'column',
		justifyContent: 'center',
		alignItems: 'center',
		width: 'auto',
		paddingHorizontal: 5,
		paddingBottom: 0,
		height: '100%',
		Radius: 5,
	},

	navBtnTitle: {
		fontSize: 14,
		textAlign: 'center',
		fontWeight: '500',
	},
})
