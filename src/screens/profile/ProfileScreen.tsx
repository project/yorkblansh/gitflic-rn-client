import { Pressable, StyleSheet, View } from 'react-native'
import React from 'react'
import { NavigationBar } from '../../components/general/NavigationBar/NavigationBar'
import { UserInfo } from '../../components/profile/UserInfo'
import { ApiHooks } from '../../app/hooks/api.hooks'
import { curriedGoToScreen } from '../../app/utils/goToScreen'
import { useNavigation } from '@react-navigation/native'
import { StuffButton } from '../../components/project/StuffButton'
import { HeaderBar } from '../../components/general/NavigationBar/HeaderBar/HeaderBar'

export const ProfileScreen = () => {
	const [user, isFetching] = ApiHooks.useCurrentUser()
	const navigation = useNavigation()
	const goToScreen = curriedGoToScreen(navigation)

	return (
		<View style={styles.wrapper}>
			<View style={styles.profile}>
				<HeaderBar screen="Profile" />

				<View style={styles.userInfo}>
					<UserInfo {...{ isFetching }} {...user} />
				</View>

				<View style={styles.userStuff}>
					<Pressable
						onPress={() => {
							goToScreen('ProjectList')
						}}>
						{({ pressed }) => (
							<StuffButton
								color={pressed ? '#303030' : '#0c76cc'}
								title="Проекты"
								icon="project"
								count={5}
								{...{ pressed }}
							/>
						)}
					</Pressable>
					<Pressable
						onPress={() => {
							goToScreen('ProjectList')
						}}>
						{({ pressed }) => (
							<StuffButton
								color="#cc690c"
								title="Компании"
								icon="company"
								count={5}
								{...{ pressed }}
							/>
						)}
					</Pressable>
					<StuffButton color="#00a380" title="Команды" icon="team" count={5} />

					{/* <Button onPress={() => goToScreen('ProjectList')} title="Проекты" />
					<Button onPress={() => goToScreen('TeamList')} title="Команды" />
					<Button onPress={() => goToScreen('CompanyList')} title="Компании" /> */}
				</View>
			</View>

			<NavigationBar screen="Profile" />
		</View>
	)
}

const styles = StyleSheet.create({
	wrapper: {
		display: 'flex',
		flexDirection: 'column',
		height: '100%',
		justifyContent: 'space-between',
		backgroundColor: '#white',
	},

	profile: {
		display: 'flex',
		flexDirection: 'column',
		alignContent: 'center',
		width: 'auto',
		height: 'auto',
	},
	userInfo: {
		display: 'flex',
		backgroundColor: 'white',
		alignItems: 'center',
		alignContent: 'center',
	},

	userStuff: {
		display: 'flex',
		flexDirection: 'column',
		alignContent: 'flex-start',
		justifyContent: 'space-between',
		backgroundColor: 'white',
		width: '100%',
		height: 'auto',
		padding: 0,
		marginVertical: 10,
		borderTopWidth: 0.5,
		borderTopColor: '#e0e0e0',
		elevation: 3,
		// shadowColor:'#686868'
		// textShadowRadius:5
		// shadowOffset: { height: 2, width: 5 },
	},
})
