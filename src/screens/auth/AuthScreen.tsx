import { Button, StyleSheet, View } from 'react-native'
import React, { useEffect, useState } from 'react'
import { AccountServices } from '../../app/services/account.services'
import { ScreenProps } from '../../interface/screen.props.interface'
import { Dimensions } from 'react-native'
import GitflicLogoWithText from '../../components/general/GitflicLogoWithText'
import { Text } from 'react-native'
import { FooterAppInfo } from '../../components/general/AppInfo'

export const AuthScreen = ({ navigation }: ScreenProps) => {
	const [isRegBtn, setRegBtnDisabled] = useState(false)
	const goToLogin = () => navigation.navigate('Login')

	useEffect(() => {
		AccountServices.isAuthorized() && navigation.navigate('Profile')
	})

	return (
		<View style={styles.origin}>
			<View style={styles.header}>
				<Text>header</Text>
			</View>

			<View style={styles.btnWrapper}>
				<View style={styles.logoWrapper}>
					<GitflicLogoWithText />
				</View>

				<Button onPress={goToLogin} color="#28a745" title="Войти" />
				<Button
					disabled={isRegBtn}
					color="#000"
					title="Регистрация (скоро будет)"
					onPress={() => setRegBtnDisabled(true)}
				/>
			</View>

			<View style={styles.footer}>
				<FooterAppInfo />
			</View>
		</View>
	)
}

const styles = StyleSheet.create({
	origin: {
		backgroundColor: 'transparent',
		display: 'flex',
		flexDirection: 'column',
		justifyContent: 'space-between',
		alignItems: 'center',
		height: Dimensions.get('screen').height - 20,
	},

	header: {
		justifyContent: 'center',
		flexDirection: 'column',
		display: 'flex',
		height: '0%',
		flex: 0,
		width: '100%',
		backgroundColor: 'transparent',
		// flexGrow: 0,
		// flexShrink: 0,
		// flexBasis: 'auto',
	},

	btnWrapper: {
		backgroundColor: '#333',
		display: 'flex',
		justifyContent: 'space-around',
		flexDirection: 'column',
		width: Dimensions.get('window').width / 1.5,
		height: 200,
		padding: 10,
		borderRadius: 10,
		// borderColor: '#cccccc',
		// borderWidth: 2,
		// borderRadius: 15,
	},
	logoWrapper: {
		display: 'flex',
		flexDirection: 'row',
		justifyContent: 'center',
	},

	footer: {
		// backgroundColor: '#c2c2c2',
		display: 'flex',
		height: 'auto',
		width: '80%',
		borderRadius: 10,
		margin: 10,
		// borderColor: '#000',
		// borderWidth: 4,
		// justifyContent: 'flex-start',
		// height: '10%',
		// flexBasis: '1',
		// flexDirection: 'row',
		// justifyContent: 'flex-end',
		// alignContent: 'center',
	},
})
