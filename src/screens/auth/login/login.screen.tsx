import React from 'react'
import { ScreenProps } from '../../../interface/screen.props.interface'
import { Platform } from 'react-native'
import { LoginScreenAndroid } from './login.android/LoginScreenAndroid'

export const LoginScreen = ({ navigation }: ScreenProps) => {
	return Platform.OS === 'android' ? (
		<LoginScreenAndroid {...{ navigation }} />
	) : (
		// require('./login.android/LoginScreenAndroid')({ navigation })
		<></>
	)
}
