/* eslint-disable react-native/no-inline-styles */
import React, { useState } from 'react'
import { StyleSheet, Text, View } from 'react-native'
import WebView, { WebViewNavigation } from 'react-native-webview'
import { WebViewNavigationEvent } from 'react-native-webview/lib/WebViewTypes'
import { OAUTH } from '../../../../app/GitFlic/gitflicInstances'
import { performAuth } from '../../../../app/services/performAuth'
import { ScreenProps } from '../../../../interface/screen.props.interface'
import { Platform } from 'react-native'

export const LoginScreenAndroid = ({ navigation }: ScreenProps) => {
	const [isLoginPhase, setPhase] = useState<'login_phase' | 'NOT_login_phase'>(
		'login_phase',
	)
	const [webViewState, setWebViewState] = useState<{ webViewState: boolean }>({
		webViewState: false,
	})
	console.log(webViewState)
	const onLoadStart = (event: WebViewNavigationEvent) => {
		const isLoading = event.nativeEvent.loading

		isLoginPhase
			? isLoading
				? setWebViewState({ webViewState: false })
				: setWebViewState({ webViewState: true })
			: setWebViewState({ webViewState: true })
	}

	const onLoadEnd = (event: WebViewNavigationEvent) => {
		const isLoading = event.nativeEvent.loading
		isLoading
			? setWebViewState({ webViewState: false })
			: setWebViewState({ webViewState: true })
		setPhase('NOT_login_phase')
	}

	const onNavigationStateChange = (event: WebViewNavigation) => {
		const linkWithAccesCode = event.url
		const isTokenQuery = event.url.search(/code/) !== -1
		isTokenQuery && setPhase('NOT_login_phase')
		isTokenQuery &&
			performAuth(linkWithAccesCode)
				.then(isAuthed =>
					isAuthed
						? navigation.navigate('Profile')
						: navigation.navigate('Home'),
				)
				.catch(er => console.log(er))
	}

	return (
		<>
			<View
				style={{
					display: 'flex',
					// display: webViewState ? 'none' : 'flex',
					...styles.loader,
				}}>
				<Text style={styles.loaderText}>
					{webViewState ? 'Авторизация через 0auth:' : 'Загружаем WebView...'}
				</Text>
			</View>

			{Platform.OS === 'android' && (
				<WebView
					useWebView2
					style={{
						// display: webViewState ? 'flex' : 'none',
						display: 'flex',
						flexDirection: 'row',
						height: 50,
					}}
					{...{ onNavigationStateChange, onLoadStart, onLoadEnd }}
					source={{ uri: OAUTH.getDeepLink() }}
					incognito
				/>
			)}
		</>
	)
}

const styles = StyleSheet.create({
	loader: {
		flexDirection: 'column',
		height: '8%',
		justifyContent: 'center',
		alignContent: 'center',
	},
	loaderText: {
		fontSize: 22,
		textAlign: 'center',
		color: '#4e4e4e',
	},
})
