import React from 'react'
import {
	StyleSheet,
	View,
	Text,
	ScrollView,
	useWindowDimensions,
} from 'react-native'
import { NavigationBar } from '../../components/general/NavigationBar/NavigationBar'
import { ApiHooks } from '../../app/hooks/api.hooks'
import { useNavigation } from '@react-navigation/native'
import { ProjectList } from '../../components/project/ProjectList'

export const HomeScreen = () => {
	const navigation = useNavigation()

	const { height } = useWindowDimensions()
	const [entitieList, isFetching] = ApiHooks.useAllProjectList()

	return (
		<View style={styles.wrapper}>
			<ScrollView style={{ height: height - 70 }}>
				{isFetching ? (
					<Text>Loading...</Text>
				) : (
					<ProjectList {...{ entitieList, navigation }} />
				)}
			</ScrollView>

			<NavigationBar screen="Home" />
		</View>
	)
}

const styles = StyleSheet.create({
	wrapper: {
		display: 'flex',
		height: '100%',
		width: 'auto',
		backgroundColor: '#e3ffee',
	},
})
