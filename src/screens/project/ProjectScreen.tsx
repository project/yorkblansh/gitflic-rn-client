import React, { useEffect } from 'react'
import { NativeStackScreenProps } from '@react-navigation/native-stack'
import { Project } from 'gitflic-api'
import { StyleSheet, Text, View } from 'react-native'
import { NavigationBar } from '../../components/general/NavigationBar/NavigationBar'
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'

import { ApiServices } from '../../app/services/api.services'
import { StuffButton } from '../../components/project/StuffButton'

type Props = NativeStackScreenProps<
	{ ProjectScreen: { project: Project } },
	'ProjectScreen'
>

export const ProjectScreen = ({ route, navigation }: Props) => {
	const { project } = route.params
	const {
		alias,
		description,
		private: isPrivate,
		title,
		httpTransportUrl,
	} = project

	const isDescription = description.trim().length > 2
	const projectLink = httpTransportUrl.substring(0, httpTransportUrl.length - 4)
	const privateProjectIcon = (
		<FontAwesomeIcon size={20} color="#646464" icon={['fas', 'lock']} />
	)
	const linkProjectIcon = (
		<FontAwesomeIcon size={20} color="#020088" icon={['fas', 'link']} />
	)

	// console.log(AccountDataStore.get())
	// const { username } = AccountDataStore.get()
	// const [a, b] = ApiHooks.useIssues({
	// 	idqp: { localId: 1, userAlias: 'yorkblansh', projectAlias: alias },
	// })
	useEffect(() => {
		ApiServices.getAllProjectList()
			// ApiServices.getIssues({
			// 	idqp: {
			// 		localId: 1,
			// 		userAlias: 'yorkblansh',
			// 		projectAlias: 'gitflic-api',
			// 	},
			// })
			.then(resp => {
				// resp.data._embedded.issueDiscussionList.map(e => {
				// 	console.log(e)
				// })
				console.log(resp.data)
			})
			.catch(er => console.log(er))
	})
	return (
		<>
			<NavigationBar props={{ project }} screen="Project" />

			<View>
				<View style={styles.head}>
					<View>
						<View>{/* icon */}</View>

						<Text style={{ color: '#757575' }}>{project.ownerAlias}</Text>
					</View>

					<View style={styles.titleView}>
						<Text style={styles.titleText}>{title}</Text>
					</View>

					{isDescription && (
						<View>
							<Text style={styles.descriptionText}>{description}</Text>
						</View>
					)}

					<View style={styles.aliasView}>
						<Text style={styles.aliasText}>{alias}</Text>
					</View>

					{isPrivate && (
						<View style={styles.privateView}>
							<View style={styles.privateViewIcon}>{privateProjectIcon}</View>
							<Text style={styles.privateText}>Приватный</Text>
						</View>
					)}

					<View style={styles.linkView}>
						<View style={styles.linkViewIcon}>{linkProjectIcon}</View>
						<Text style={styles.linkText}>{projectLink}</Text>
					</View>
				</View>

				<View
					//TODO это надо переместить в отдельный компонент
					style={styles.projectStuff}>
					<StuffButton
						color="#41ac21"
						title="Проблемы"
						icon="issue"
						count={0}
					/>
					<StuffButton
						color="#c20061"
						title="Запрсы слияния"
						icon="pullRequest"
						count={0}
					/>
					<StuffButton color="#333" title="Релизы" icon="release" count={0} />
				</View>

				<View
					style={{
						backgroundColor: '#e9e9e9',
						// margin: 15,
						// borderRadius: 15,
						paddingHorizontal: 0,
						borderColor: '#b9b9b9',
						borderWidth: 1.5,
						// elevation: 5,
					}}>
					<View style={{ display: 'flex', flexDirection: 'row' }}>
						<View style={{ width: '100%' }}>
							<StuffButton color="#bdbdbd" title="Ветка" icon="branch">
								<View
									style={{
										backgroundColor: '#fff',
										width: 'auto',
										display: 'flex',
										flexDirection: 'row',
										// justifyContent: 'center',
										paddingLeft: 15,
										borderRadius: 25,
										margin: 3,
									}}>
									<StuffButton
										color="#bdbdbd"
										title="master"
										icon="branch"
										size={30}
									/>
								</View>
							</StuffButton>
						</View>
					</View>
					<StuffButton color="#bdbdbd" title="Код" icon="code" />
				</View>
			</View>
		</>
	)
}

const styles = StyleSheet.create({
	head: {
		height: 'auto',
		display: 'flex',
		flexDirection: 'column',
		padding: '5%',
		backgroundColor: '#ebebea',
		borderBottomColor: '#969696',
		borderBottomWidth: 0.5,
	},

	titleView: {
		display: 'flex',
		paddingVertical: 10,
	},
	titleText: {
		fontSize: 30,
		color: '#000',
		fontWeight: '700',
	},

	descriptionText: {
		color: '#757575',
		fontSize: 18,
	},

	aliasView: {
		// backgroundColor:'#000',
		display: 'flex',
		flexDirection: 'row',
		justifyContent: 'flex-start',
		// alignContent: 'center',
		alignSelf: 'auto',
		// textAlign: 'start',
		// width: '50%',
	},

	aliasText: {
		paddingBottom: 2,
		backgroundColor: '#d3d3d3',
		color: '#000000',
		display: 'flex',
		fontSize: 15,
		paddingHorizontal: 5,
		marginVertical: 15,
		borderRadius: 5,
	},

	privateView: {
		display: 'flex',
		flexDirection: 'row',
		marginVertical: 5,
		// backgroundColor: '#faaa',
	},
	privateViewIcon: {
		display: 'flex',
		flexDirection: 'column',
		justifyContent: 'center',
		alignContent: 'center',
		paddingHorizontal: 5,
	},
	privateText: {
		fontSize: 15,
	},

	linkView: {
		display: 'flex',
		flexDirection: 'row',
		marginTop: 5,

		// backgroundColor: '#faaa',
	},
	linkViewIcon: {
		display: 'flex',
		flexDirection: 'column',
		justifyContent: 'center',
		alignContent: 'center',
		paddingHorizontal: 5,
	},
	linkText: {
		fontSize: 15,
		fontWeight: '500',
		color: '#020088',
	},

	projectStuff: {
		display: 'flex',
		backgroundColor: '#f3f3f3',
		height: 'auto',
		// elevation:2,
		marginBottom: 25,
		// flexDirection
	},
})
