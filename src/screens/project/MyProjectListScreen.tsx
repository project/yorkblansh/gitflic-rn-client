import React from 'react'
import { Text, View } from 'react-native'
import { ApiHooks } from '../../app/hooks/api.hooks'
import { NavigationBar } from '../../components/general/NavigationBar/NavigationBar'
import { ProjectList } from '../../components/project/ProjectList'

export const MyProjectListScreen = () => {
	const [entitieList, isFetching] = ApiHooks.useMyProjectList()

	return (
		<View>
			<NavigationBar screen="ProjectList" />

			{isFetching ? (
				<Text>Loading...</Text>
			) : (
				<ProjectList {...{ entitieList }} />
			)}
		</View>
	)
}
