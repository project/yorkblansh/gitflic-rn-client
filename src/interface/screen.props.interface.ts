import { SCREENS } from '../App'

export interface navigationType {
	navigate: (screenName: keyof typeof SCREENS, props?: any) => void
}

export interface ScreenProps {
	navigation: navigationType
}
