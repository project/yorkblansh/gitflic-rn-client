# Взаимодействие с API Gitflic

Что бы начать работать с api GitFlic в приложении необходимо:

### 1. Создать токен oauth:

\
Для того что бы создать токен, перейдите в настройки профиля

<center>

![Image alt](https://gitflic.ru/project/yorkblansh/gitflic-rn-client/blob/raw?file=docs%2Fimg%2Fgo-to-settings.png&commit=6343cbc2056f47e2e0e69f704575ef332de988ce)

</center>

### 2. Перейти во вкладку **"токены"**, создать токен:

<center>

![Image alt](https://gitflic.ru/project/yorkblansh/gitflic-rn-client/blob/raw?file=docs%2Fimg%2Foauth-panel.png&commit=d0df252a716addfce707b87d29248debf67ec042)

</center>

### 3. Далее ввести имя токена и выбрать разрешения которые необходимы:

<center>

![Image alt](https://gitflic.ru/project/yorkblansh/gitflic-rn-client/blob/raw?file=docs%2Fimg%2Fcreate-token.png&commit=d0df252a716addfce707b87d29248debf67ec042)

</center>

### 4. **ВАЖНО!** После создания токена _(не перезагружайте страницу и не переходите по другим ссылкам в gitflic в этот момент, иначе токен пропадет)_ вы получите следующее уведомление:

<center>

![Image alt](https://gitflic.ru/project/yorkblansh/gitflic-rn-client/blob/raw?file=docs%2Fimg%2Ftoken-copy.png&commit=d0df252a716addfce707b87d29248debf67ec042)

</center>

Сохраните токен, он понадобится далее.

### 5. Далее Создать файл:

```
<ПАПКА_ПРОЕКТА>/src/app/api/gitflic_token.json
```

### 6. Заполнить файл следующим образом:

```json
{
	"oauth_token": "<ВАШ_OAUTH_TOKEN>"
}
```
